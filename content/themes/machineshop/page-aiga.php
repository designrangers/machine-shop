<?php
/*
 Template Name: AIGA
*/
?>

<?php get_header(); ?>
<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" class="" role="article">
			<?php $aigaherobg = get_field('aiga_hero_bkd'); ?>
			<section id="home" class="fullscreen-hero cf" style="background-image: url('<?php echo $aigaherobg; ?>');">
				<div class="machine-hero timed">
					<div class="wrap cf hero-text animated fadeInUp">
						<?php $logoimage = get_field('aiga_hero_logo'); ?>
						<img class="hero-logo" src="<?php echo $logoimage['url']; ?>" alt="<?php echo $logoimage['alt']; ?>" />
						<h1><?php the_field('aiga_hero_title') ?></h1>
						<?php the_field('aiga_hero_text') ?>
					</div>
				</div>
			</section>
		
			<section id="about" class="aiga-intro">
				<div class="wrap cf">
					<div class="about-text d-5of5 t-5of5">
						<?php the_field('aiga_intro') ?>
					</div>
				</div>
			</section>
			<section id="events" class="machine-space aiga-featured">
				<div class="wrap cf">
					<?php 
						$featuredimg1 = get_field('featured_image_1');
						$featuredimg2 = get_field('featured_image_2'); ?>

					<h2><?php the_field('featured_section_title'); ?></h2>
					<div class="d-1of2 t-1of2 featured-item-left">
						<img src="<?php echo $featuredimg1['url']; ?>" alt="<?php echo $featuredimg1['alt']; ?>" />
						<?php the_field('featured_text_1'); ?>
					</div>
					<div class="d-1of2 t-1of2 last-col featured-item-right">
						<img src="<?php echo $featuredimg2['url']; ?>" alt="<?php echo $featuredimg2['alt']; ?>" />
						<?php the_field('featured_text_2'); ?>
					</div>					

				</div>
			</section>
			<section id="calendar" class="machine-events aiga-events">
				<div class="wrap cf">
					<div class="d-all m-all">
						<h3>Event Calendar</h3>
					<?php
						// The Query
						$args = array(
							  'post_status'=>'publish',
							  'post_type'=>array(TribeEvents::POSTTYPE),
							  'posts_per_page'=>10,
							  //order by startdate from newest to oldest
							  'meta_key'=>'_EventStartDate',
							  'orderby'=>'_EventStartDate',
							  'order'=>'ASC',
							  //required in 3.x
							  'eventDisplay'=>'upcoming',
							  'tax_query' => array(
							      array(
							          'taxonomy' => 'tribe_events_cat',
							          'field' => 'slug',
							          'terms' => 'aiga',
							          'operator' => 'IN'
							      ),
							  )
						);
						$machine_events = new WP_Query( $args );
						// The Loop
						if ( $machine_events->have_posts() ) {

							echo '<div class="accordion event-accordion">';
							
							while ( $machine_events->have_posts() ) {
								$machine_events->the_post(); ?>

									<h3 class="event-heading"><?php the_title(); echo '<span class="event-date">' . tribe_get_start_date($pageID, true, 'n.j.y') . '</span>'; ?></h3>
									<div>
										<div class="d-2of3 event-overview">
											<?php the_excerpt(); ?>
										</div>
										<div class="d-1of3 event-info">
											<p><strong>Where:</strong><?php echo tribe_get_venue(); ?>
											<p><strong>When:</strong><?php echo tribe_get_start_time() . ' - ' . tribe_get_end_time(); ?></p>
												<?php if(tribe_get_formatted_cost()) : ?>
												<p><strong>Cost:</strong><?php echo tribe_get_formatted_cost(); ?></p>
											<?php endif; ?>
										</div>
									</div>								
								
							<?php }
								echo '</div>';
							?>
					</div>
					<?php 
						} else { ?>
							<ul class="events-list">
								<li><a href="#">No upcoming events</a></li>
							</ul>
					<?php }
						/* Restore original Post Data */
						wp_reset_postdata();
					?>
				</div>
			</section>
			<!-- Email Signup -->
			<section id="calendar" class="aiga-subscribe">
				<div class="wrap cf">
					<div class="m-all t-all d-2of3">
						<p class="aiga-subscribe-text"><?php the_field('email_subscribe_text'); ?></p>
					</div>
					<div class="m-all t-all d-1of3">
						<?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $ajax = true ); ?>
					</div>	
				</div>
			</section>			
			<section id="contact" class="machine-details aiga-contact cf">
				<div class="wrap cf">
					<h2><?php echo the_field('connect_section_title'); ?></h2>
					<?php if( have_rows('connect_column') ): ?>

						<?php while( have_rows('connect_column') ): the_row(); 
							$image = get_sub_field('connect_icon');
							?>
							<div class="d-1of3 t-1of3 aiga-connect-item">
								<img class="machine-icon" src="<?php echo $image['url']; ?>" />
		    					<h3><?php the_sub_field('connect_title'); ?></h3>
		  						<p><?php the_sub_field('connect_text'); ?></p>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>

				</div>
			</section>

			<section id="visit" class="machine-contact">
				<div class="wrap cf">
					<h2>Find Us</h2>
				</div>
				<div class="Flexible-container">
					<div id="map-canvas" />
				</div>
			</section>				

	</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>



<?php get_footer(); ?>
