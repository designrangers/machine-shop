<?php
/*
 Template Name: Homepage
*/
?>

<?php get_header(); ?>
<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" class="" role="article">

			<section id="home" class="machine-hero-wrap cf">
				<?php $herobg = get_field('hero_image'); ?>
				<div class="machine-hero timed" style="background-color: #343436; background-image: url('<?php echo $herobg; ?>');">
					<div class="wrap cf hero-text animated fadeInUp">
						<?php echo the_field('hero_content'); ?>
					</div>
				</div>
			</section>
		
			<section id="about" class="machine-about cf">
				<div class="wrap cf">
					<div class="about-image d-2of5 t-2of5">
						<?php 
 
							$image = get_field('home_about_image');
							 
							if( !empty($image) ): ?>
							 
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							 
						<?php endif; ?>
					</div>
					<div class="about-text d-3of5 t-3of5">
						<h2><?php echo the_field('home_about_title'); ?></h2>
						<?php echo the_field('home_about_content'); ?>
					</div>
				</div>
			</section>
			<section class="machine-details cf">
				<div class="wrap cf">
					<h2><?php echo the_field('bolts_section_title'); ?></h2>

					<?php if( have_rows('bolts_column') ): ?>
						<?php while( have_rows('bolts_column') ): the_row(); 
							$image = get_sub_field('bolts_icon');
							?>
							<div class="d-1of3 t-1of3">
								<img class="machine-icon" src="<?php echo $image['url']; ?>" />
		    					<h3><?php the_sub_field('bolts_title'); ?></h3>
		  						<p><?php the_sub_field('bolts_text'); ?></p>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>

				</div>
			</section>
			<section id="team" class="machine-team">
				<div class="wrap cf">
				<h2><?php the_field('team_title'); ?></h2>
					<p class="intro"><?php the_field('team_subtitle'); ?></p>

					<?php if( have_rows('team_bio') ): ?>
						<?php while( have_rows('team_bio') ): the_row(); 
							$image = get_sub_field('company_icon');
							?>
							<div class="d-1of3 t-1of2 machine-partner">
								<a href="<?php the_sub_field('company_website'); ?>" target="_blank"><img class="machinist-logo" src="<?php echo $image['url']; ?>" /></a>
		    					<h3><?php the_sub_field('company_title'); ?></h3>
								<p><?php the_sub_field('company_description'); ?></p>
								<?php if( get_sub_field('company_website') ) : ?>
									<a href="<?php the_sub_field('company_website'); ?>" target="_blank"><?php the_sub_field('company_title'); ?> Website</a>
								<?php endif; ?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>

				</div>
			</section>
			<section id="thespace" class="machine-space">
				<div class="wrap cf">
					<h2>The Space</h2>
					<div class="machine-slideshow">
						<?php echo do_shortcode( '[wooslider  slider_type="slides" limit="10" smoothheight="true"]' ); ?>
						
					</div>
				</div>
			</section>
			<section id="events" class="machine-events">
				<div class="wrap cf">
					<h2>Upcoming Events</h2>
					<div class="d-2of3 t-all">
					<?php
						// The Query
						$args1 = array(
							  'post_status'=>'publish',
							  'post_type'=>array(TribeEvents::POSTTYPE),
							  'posts_per_page'=>1,
							  //order by startdate from newest to oldest
							  'meta_key'=>'_EventStartDate',
							  'orderby'=>'_EventStartDate',
							  'order'=>'ASC',
							  //required in 3.x
							  'eventDisplay'=>'custom',
							    //query events by category
							  'tax_query' => array(
							      array(
							          'taxonomy' => 'tribe_events_cat',
							          'field' => 'slug',
							          'terms' => 'featured',
							          'operator' => 'IN'
							      ),
							  )
						);
						$machine_featured = new WP_Query( $args1 );
						// The Loop
						if ( $machine_featured->have_posts() ) {							
							while ( $machine_featured->have_posts() ) {
								$machine_featured->the_post(); ?>
						<div class="d-1of3 t-1of3">
							<?php echo tribe_event_featured_image($pageID, 'bones-thumb-400'); ?>
						</div>
						<div class="d-2of3 t-2of3 event-description">
							<h3><?php the_title(); ?></h3>						
							<h4 class="featured-date">
								<?php echo tribe_get_start_date(); ?>
							</h4>	
							<?php the_excerpt(); ?>
							<a class="machine-button" href="<?php the_permalink(); ?>">Full Event Info</a>
						</div>
						<?php 
								}
							} else {
								// no posts found
							}
							/* Restore original Post Data */
							wp_reset_postdata();
						?>
					</div>
					<div class="d-1of3 m-all events-more">
						<h3>More Events</h3>
					<?php
						// The Query
						$args = array(
							  'post_status'=>'publish',
							  'post_type'=>array(TribeEvents::POSTTYPE),
							  'posts_per_page'=>4,
							  //order by startdate from newest to oldest
							  'meta_key'=>'_EventStartDate',
							  'orderby'=>'_EventStartDate',
							  'order'=>'ASC',
							  //required in 3.x
							  'eventDisplay'=>'custom',
							  'tax_query' => array(
							      array(
							          'taxonomy' => 'tribe_events_cat',
							          'field' => 'slug',
							          'terms' => 'featured',
							          'operator' => 'NOT IN'
							      ),
							  )
						);
						$machine_events = new WP_Query( $args );
						// The Loop
						if ( $machine_events->have_posts() ) {

							echo '<ul class="events-list">';
							
							while ( $machine_events->have_posts() ) {
								$machine_events->the_post(); ?>

								<li><a href="<?php the_permalink(); ?>"><span class="event-date"><?php echo tribe_get_start_date($pageID, false, "M d"); ?></span><?php the_title(); ?></a></li>
							<?php }
								echo '</ul>';
							?>
					</div>
					<?php 
						} else { ?>
							<ul class="events-list">
								<li><a href="#">No upcoming events</a></li>
							</ul>
					<?php }
						/* Restore original Post Data */
						wp_reset_postdata();
					?>
				</div>
			</section>

			<section id="inquire" class="machine-join">
				<h2><?php the_field('inquire_title'); ?></h2>
				<?php the_field('inquire_content'); ?>
					<?php gravity_form(1, $display_title=false, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true, $tabindex); ?>
				</div>
			</section>

			<section id="visit" class="machine-contact">
				<div class="wrap cf">
					<h2>Find Us</h2>
				</div>
				<div class="Flexible-container">
					<div id="map-canvas" />
				</div>
			</section>				

	</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>



<?php get_footer(); ?>
