<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // Google Chrome Frame for IE ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>


		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // Open Graph Meta tags, you know... so Facebook likes us ?>
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/library/images/machine-shop-logo-square.png" />
		<meta property="og:title" content="The Machine Shop"/>
		<meta property="og:description" content="The Machine Shop is the collaborative workspace co-founded by four independent creative studios as a shared studio space, with room for a few like-minded creative folks to join us along the way. Located in downtown Colorado Springs, this former automotive garage is now home to artists, architects, developers, designers, writers and video wizards."/>
		<meta property="og:url" content="http://www.jointhemachine.com"/>
		<meta property="og:site_name" content="The Machine Shop"/>

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // Typekit ?>
		<script type="text/javascript" src="//use.typekit.net/xcw6kyx.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?>>

		<div id="container">

			<header class="header" role="banner">

				<div id="inner-header" class="wrap cf">

					

					<a id="logo" href="<?php echo home_url(); ?>" rel="nofollow" data-scroll ><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/machine-shop-logo.png"></a>
					<nav class="nav-collapse" role="navigation">
					<?php 
					$machinehome=is_front_page();
					$aigahome=is_page('aigacos');

					if ($machinehome) { ?>
						<ul class="nav top-nav cf">
							<li><a href="#about" data-scroll>About</a></li>
							<li><a href="#team" data-scroll>Team</a></li>
							<li><a href="#thespace" data-scroll>The Space</a></li>
							<li><a href="#events" data-scroll>Events</a></li>
							<li><a href="#inquire" data-scroll>Inquire</a></li>
							<li><a href="#visit" data-scroll>Visit</a></li>
						</ul>
					<?php }

					elseif ($aigahome) { ?>
							<ul class="nav top-nav cf">
							<li><a href="#about" data-scroll>About</a></li>
							<li><a href="#events" data-scroll>Events</a></li>
							<li><a href="#calendar" data-scroll>Calendar</a></li>
							<li><a href="#contact" data-scroll>Connect</a></li>
							<li><a href="/" data-scroll>The Machine Shop</a></li>
						</ul>
	
					<?php }					

					else { ?>
							<ul class="nav top-nav cf">
							<li><a href="/#about" data-scroll>About</a></li>
							<li><a href="/#team" data-scroll>Team</a></li>
							<li><a href="/#thespace" data-scroll>The Space</a></li>
							<li><a href="/#events" data-scroll>Events</a></li>
							<li><a href="/#inquire" data-scroll>Inquire</a></li>
							<li><a href="/#visit" data-scroll>Visit</a></li>
						</ul>
	
					<?php }

					/* WP menu – Will implement at a later date with a custom walker
						wp_nav_menu(array(
    					'container' => false,                           // remove nav container
    					'container_class' => 'menu cf',                 // class of container (should you choose to use it)
    					'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
    					'menu_class' => 'nav top-nav cf',               // adding custom nav class
    					'theme_location' => 'main-nav',                 // where it's located in the theme
    					'before' => '',                                 // before the menu
        			'after' => '',                                  // after the menu
        			'link_before' => '',                            // before each link
        			'link_after' => '',                             // after each link
        			'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => ''                             // fallback function (if there is one)
						)); 
					*/	
					?>

					</nav>

				</div>

			</header>
